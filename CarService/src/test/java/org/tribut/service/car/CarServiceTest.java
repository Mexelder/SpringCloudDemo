package org.tribut.service.car;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.tribut.service.car.dto.CarResponse;
import org.tribut.service.car.model.Car;
import org.tribut.service.car.repository.CarRepository;
import org.tribut.service.car.service.CarService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class CarServiceTest {

    @InjectMocks
    private CarService carService;

    @Mock
    private CarRepository carRepository;

    public CarServiceTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllCars() {
        // Arrange
        Car car1 = new Car("id1", "Car1", "Color1", BigDecimal.valueOf(1000));
        Car car2 = new Car("id2", "Car2", "Color2", BigDecimal.valueOf(2000));

        List<Car> carList = Arrays.asList(car1, car2);

        when(carRepository.findAll()).thenReturn(carList);

        // Act
        List<CarResponse> carResponses = carService.getALlCars();

        // Assert
        assertEquals(carList.size(), carResponses.size());
        assertEquals("id1", carResponses.get(0).getId());
        assertEquals("Car1", carResponses.get(0).getModel());
        assertEquals("Color1", carResponses.get(0).getColor());
        assertEquals(BigDecimal.valueOf(1000), carResponses.get(0).getPrice());

        assertEquals("id2", carResponses.get(1).getId());
        assertEquals("Car2", carResponses.get(1).getModel());
        assertEquals("Color2", carResponses.get(1).getColor());
        assertEquals(BigDecimal.valueOf(2000), carResponses.get(1).getPrice());
    }
}
